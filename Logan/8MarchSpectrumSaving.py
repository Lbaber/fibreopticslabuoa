from instrumentClasses import ANDOOSA,FPM8210, lecroyWaverunner
import numpy as np
import time
import matplotlib.pyplot as pp
from mpl_toolkits.mplot3d import Axes3D
import visa
import json
import os

class testManager(object):
	

	def __init__(self):
		#Start instruments
		rm = visa.ResourceManager()
		
		self.osa = ANDOOSA(rm)
		self.pm = FPM8210(rm)
		self.OScope = lecroyWaverunner()
		
	def defineWL(self, center, span, sample):
		self.osa.centerWL = center
		self.osa.span = span
		self.osa.samplePoints = sample
		
		time.sleep(2)
		
	def readSpectrum(self):
		self.osa.singleSweep()
		time.sleep(2)

		data = self.osa.getAData().split(',')[1:]
	
		data_float = []
		for val in data:
			data_float.append(float(val))
		
		return data_float
		
	def readWavelengths(self):
		wavelengths = list(self.osa.getWavelengths())
		return wavelengths
		
	def getPower(self):
		power = float(self.pm.power)
		return power
		
	def getTimeDomain(self):
		self.OScope.saveYVals(r'C:\Users\lbab853\Desktop\PhDCode\data\scan1')
		time.sleep(2)
		data = np.fromfile(r'C:\Users\lbab853\Desktop\PhDCode\data\scan1', dtype='int8', count=-1)
	
		newData = [float(val) for val in data]		
		
		return newData
		
class savingManager(object):
	
	def __init__(self, path="C:\Users\lbab853\Desktop\PhDCode\data\\", filename='scan_'):
		#define constants
		self.path = path
		self.filename = filename
		self.dataFormat = '''[{}]'''
		
		#Converts data to jsonData so we can edit like a dictionary
		self.data = json.loads(self.dataFormat)
		
	def addData(self, key, data):
		self.data[0][key] = data
		
	def saveFile(self):
		self.jsonData = json.dumps(self.data, indent=4)
		
		suffix = '1'
		#Determines the suffix
		filePath = self.path + self.filename + suffix
		
		while os.path.isfile(filePath):
			#File already exists
			suffix = str(int(suffix) + 1)
			filePath = self.path + self.filename + suffix
		
		
		with open(self.path + self.filename + suffix, 'w+') as f:
			f.write(self.jsonData)		

class plottingManager(object):
	
	def __init__(self, path = "C:\Users\lbab853\Desktop\PhDCode\data\\", fileName='plot_'):
		self.path = path
		self.fileName = fileName
		
	def matplotlibSettings():
		pass		
		
	def plotData(self, xArr, yArr):
			
		suffix = '1'
		extension = '.png'
		#Determines the suffix
		filePath = self.path + self.fileName + suffix + extension
		
		while os.path.isfile(filePath):
			#File already exists
			suffix = str(int(suffix) + 1)
			filePath = self.path + self.fileName + suffix + extension
		
		pp.plot(xArr, yArr)
		#pp.savefig(filePath, bbox_inches='tight')
		
	def create3DGraph(self, xArr, yArr, zArr):
		fig = pp.figure()
		ax = fig.add_subplot(111, projection='3d')
		
		X,Y = np.meshgrid(xArr, yArr)
		
		surf = ax.plot_wireframe(X,Y,zArr,rstride=1, cstride=100000)
	
		pp.tight_layout()
		
		pp.show()
		
	def graphOfDCFSpectra(self):
		loadMan = loadingManager()
		data = loadMan.loadMultipleData("C:\Users\lbab853\Desktop\PhDCode\data\Calmar\\", 'scan_')

		laserPowers = np.arange(0.03,0.78,0.03)
		
		wavelengths = data['scan_1']['wavelengths']
		
		spectrumInt = np.empty([len(laserPowers),len(wavelengths)])
		
		
		for count,dataFile in enumerate(data):
			for newCount, val in enumerate(data['scan_{}'.format(count+1)]['spectrumIntensity']):
				if val < -70:
					data['scan_{}'.format(count+1)]['spectrumIntensity'][newCount] = -70

		for count,dataFile in enumerate(data):
			spectrumInt[count] = data['scan_{}'.format(count+1)]['spectrumIntensity']
			
		self.create3DGraph(wavelengths,laserPowers, spectrumInt)
		
	def graphOfCalmarSpectra(self):
		loadMan = loadingManager()
		data = loadMan.loadMultipleData("C:\Users\lbab853\Desktop\PhDCode\data\Calmar\\", 'scan_')

		laserPowers = [0.02,0.1,0.2,0.25,0.3] + list(np.arange(0.32,0.8,0.02))
		
		wavelengths = data['scan_1']['wavelengths']
		
		spectrumInt = np.empty([len(laserPowers),len(wavelengths)])
		
		
		for count,dataFile in enumerate(data):
			for newCount, val in enumerate(data['scan_{}'.format(count+1)]['spectrumIntensity']):
				if val < -70:
					data['scan_{}'.format(count+1)]['spectrumIntensity'][newCount] = -70

		for count,dataFile in enumerate(data):
			spectrumInt[count] = data['scan_{}'.format(count+1)]['spectrumIntensity']
			
		self.create3DGraph(wavelengths,laserPowers, spectrumInt)
		
		
	def graphOfDCFTimeDomain(self):
		loadMan = loadingManager()
		data = loadMan.loadMultipleData("C:\Users\lbab853\Desktop\PhDCode\data\DCFv2\\", 'scan_')

		laserPowers = np.arange(0.03,0.78,0.03)
		
		dataLength = len(data['scan_1']['timeDomainIntensity'])
		
		yAxis = range(dataLength)
		
		timeInt = np.empty([len(laserPowers),len(yAxis)])
		
		for count,dataFile in enumerate(data):
			timeInt[count] = data['scan_{}'.format(count+1)]['timeDomainIntensity']
			
		pp.plot(yAxis, data['scan_15'.format(count+1)]['timeDomainIntensity'])
			
		print list(timeInt)
		print np.shape(timeInt)
		self.create3DGraph(yAxis,laserPowers, timeInt)
		
	def graphSelectedCalmarSpectra(self):
		loadMan = loadingManager()
		data = loadMan.loadMultipleData("C:\Users\lbab853\Desktop\PhDCode\data\Calmar\\", 'scan_')

		#laserPowers = [0.02,0.1,0.2,0.25,0.3] + list(np.arange(0.32,0.8,0.02))
		laserPowers = [0.44, 0.52, 0.60 ,0.62,0.70,0.78]
		
		wavelengths = data['scan_1']['wavelengths']
		
		spectrumInt = np.empty([len(laserPowers),len(wavelengths)])
		
		selectedScans = ['12', '16','20','21', '25', '29']
		for scanNumb in selectedScans:
			for newCount, val in enumerate(data['scan_{}'.format(scanNumb)]['spectrumIntensity']):
				if val < -70:
					data['scan_{}'.format(scanNumb)]['spectrumIntensity'][newCount] = -70

		for count, scanNumb in enumerate(selectedScans):
			spectrumInt[count] = data['scan_{}'.format(scanNumb)]['spectrumIntensity']
			
		self.create3DGraph(wavelengths,laserPowers, spectrumInt)

	def graphSelectedDCFSpectra(self):
		loadMan = loadingManager()
		data = loadMan.loadMultipleData("C:\Users\lbab853\Desktop\PhDCode\data\Calmar\\", 'scan_')

		#laserPowers = [0.02,0.1,0.2,0.25,0.3] + list(np.arange(0.32,0.8,0.02))
		laserPowers = [0.44, 0.52, 0.60 ,0.62,0.70,0.78]
		
		wavelengths = data['scan_1']['wavelengths']
		
		spectrumInt = np.empty([len(laserPowers),len(wavelengths)])
		
		selectedScans = ['12', '16','20','21', '25', '29']
		for scanNumb in selectedScans:
			for newCount, val in enumerate(data['scan_{}'.format(scanNumb)]['spectrumIntensity']):
				if val < -70:
					data['scan_{}'.format(scanNumb)]['spectrumIntensity'][newCount] = -70

		for count, scanNumb in enumerate(selectedScans):
			spectrumInt[count] = data['scan_{}'.format(scanNumb)]['spectrumIntensity']
			
		self.create3DGraph(wavelengths,laserPowers, spectrumInt)	
		
		
		
		
class loadingManager(object):
	
	def __init__(self, path = "C:\Users\lbab853\Desktop\PhDCode\data\\", fileName='scan_', suffix = '1'):
		self.path = path
		self.fileName = fileName
		self.suffix = suffix
		
		self.filePath = self.path + self.fileName + self.suffix 
	
	def loadData(self, filePath):
		#Opens the data file for reading. Will just read in the whole file and then
		#Each indiviudal attribute will have to be read out elsewhere.
		#Could implement just getting the one attribute in here.
	
		with open(filePath, 'r+') as f:
			data = json.load(f)
	
		return data[0]
		
	def loadMultipleData(self, fileDir, prefix):
		#Opens all datafiles in the directory for reading.
		#Will use a prefix to find all file names with the prefix.
		data = {}
	
		#Gets a list of all the files in the directory
		fileList = [f for f in os.listdir(fileDir) if os.path.isfile(os.path.join(fileDir, f))]
		
		#Checks if the files match the prefix
		onlyPrefix = [f for f in fileList if f.find(prefix) != -1]

		for f in onlyPrefix:
			fullPath = os.path.join(fileDir, f)
			data[f] = self.loadData(fullPath)
	
		return data
		
if __name__ == '__main__':
	
	print 'gathering data...'
	
	#testMan = testManager()
	#saveMan = savingManager()
	plotMan = plottingManager()
	loadMan = loadingManager()
	'''
	#Get data from OSA
	#testMan.defineWL(1550, 50, 1001)
	yVals = testMan.readSpectrum()
	xVals = testMan.readWavelengths()
	
	#Get data from power meter
	power = testMan.getPower()
	
	#Get data from OScope
	timeDomainIntensity = testMan.getTimeDomain()
	
	print list(timeDomainIntensity)
	
	saveMan.addData('power', power)
	saveMan.addData('wavelengths', xVals)
	saveMan.addData('spectrumIntensity', yVals)
	
	saveMan.addData('timeDomainIntensity', timeDomainIntensity)
	
	saveMan.saveFile()

	
	plotMan.plotData(xVals,yVals)
	
	plotMan.plotData(range(len(timeDomainIntensity)),timeDomainIntensity)
	

	for key in data:
		scan = data[key]
		print scan
		plotMan.plotData(scan['wavelengths'], scan['spectrumIntensity'])
		
		pp.show()
		
	
	scanList = []
	for i in range(23, 29):
		scanList.append('scan_' + str(i))
		
	
	data = loadMan.loadMultipleData("C:\Users\lbab853\Desktop\PhDCode\data\DCFv2\\", 'scan_')
	
	scanList = ['calmar\scan_20', 'DCFv2\scan_20']
	
	
	selectedScanList = [os.path.join("C:\Users\lbab853\Desktop\PhDCode\data\\", fileName) for fileName in scanList]
	
	for scan in selectedScanList:
		data = loadMan.loadData(scan)
		plotMan.plotData(data['wavelengths'], data['spectrumIntensity'])
	'''
	
	#plotMan.graphOfCalmarSpectra()
	plotMan.graphSelectedCalmarSpectra()
	