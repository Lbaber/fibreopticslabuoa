#Import statements
import visa
import time
import numpy as np
import matplotlib.pyplot as pp
import vxi11

#Class for the ITC4001 Laser diode driver.
class ITC4001(object):
	#Set up the connection to the instrument
	#Note, I have hard coded the USB address of the laser diode, you may have to
	#look at the LD module in "Remote options" if this address is wrong and change it accordingly 
	def __init__(self, rm):
		self.instr = rm.open_resource("USB0::0x1313::0x804A::M00271786::INSTR")

	#------ITC4001 Methods-------#
	
	#Turns LD on or off
	@property
	def LD_state(self):
		s = self.instr.query("OUTP:STAT?")
		return s
		
	@LD_state.setter
	def LD_state(self, state):
		self.instr.write("OUTP:STAT {}".format(state))
	
	#LD current set point. Units are Amps
	@property
	def LD_current(self):
		s = self.instr.query("SOUR:CURR:LEV:AMPL?")
		return s
		
	@LD_current.setter
	def LD_current(self, current):
		self.instr.write("SOUR:CURR:LEV:AMPL {}".format(current))
	
	#LD power set point. Units are Watts
	@property
	def LD_power(self):
		s = self.instr.query("SOUR:POW:LEV:AMPL?")
		return s
	@LD_power.setter
	def LD_power(self, power):
		self.instr.write("SOUR:POW:LEV:AMPL {}".format(power))
	
	#LD mode is either CURRent or POWer
	@property
	def LD_mode(self):
		s = self.instr.query("SOUR:FUNC:MODE?")
		return s
	@LD_mode.setter
	def LD_mode(self, mode):
		self.instr.write("SOUR:FUNC:MODE {}".format(mode))

	#Turns the TEC on or off
	@property
	def TEC_state(self):
		s = self.instr.query("OUTP2:STAT?")
		return s
		
	@TEC_state.setter
	def TEC_state(self, state):
		self.instr.write("OUTP2:STAT {}".format(state))
	
	#A command to turn off the Laser Diode (Run this at end of script as you don't want to leave the diode on)
	@property
	def idle(self):
		self.LD_current = 0
		self.LD_state = 0

		
#Class for the FPM8210 Power Meter
class FPM8210(object):

	#Have hard coded address of power meter, can look at GPIO address on instrument
	def __init__(self, rm):
		self.instr = rm.get_instrument("GPIB0::5::INSTR")
		
	#Gets power reading from PM
	@property
	def power(self):
		s = self.instr.query("POW?")
		#Convert unicode return to a float
		s = float(s)
		return s
	
	#Sets filter to different speeds
	@property
	def filter(self):
		s = self.instr.query("FILT?")
		return s
		
	@filter.setter
	def filter(self, speed):
		if speed == 'FAST' or speed == 'fast' or speed == 'Fast':
			print 'fast'
		elif speed == 'MEDIUM' or speed == 'medium' or speed == 'Medium' or speed == 'MED':
			print 'medium'
		elif speed == 'SLOW' or speed == 'slow' or speed == 'Slow':
			print 'slow'
		else:
			print 'Sorry, filter options are FAST, SLOW, or MEDIUM (e.g. PM.filter = FAST)'			

			
#Class for MS9710 Optical Spectrum Analyser
class MS9710(object):
	def __init__(self, rm):
		#Have hard coded address of OSA, can look at GPIO address on instrument
		self.instr = rm.get_instrument("GPIB0::8::INSTR")
		#Set the time out to 30 seconds as reading data takes a long time
		self.instr.timeout = 30000
		
	#Define the centre wavelength. Units in nm
	@property
	def centerWavelength(self):
		s = self.instr.query("CNT?")
		#Convert unicode return to a float
		s = float(s)
		return s

	@centerWavelength.setter
	def centerWavelength(self, wave):
		s = self.instr.write("CNT {}".format(wave))
		
	#Define the span. Units in nm
	@property	
	def span(self):
		s = self.instr.query("SPN?")
		#Convert unicode return to a float
		s = float(s)
		return s
		
	@span.setter
	def span(self, span):
		s = self.instr.write("SPN {}".format(span))
	
	#Define the reference level. Units are in dBm
	@property
	def refLevel(self):
		s = self.instr.query("RLV?")
		return s
		
	@refLevel.setter
	def refLevel(self, refLevel):
		s = self.instr.write("RVL {}".format(refLevel))
	
	#Define the number of sampling points.
	@property
	def samplePoints(self):
		s = self.instr.query("MPT?")
		#Convert unicode return to an int
		s = int(s)
		return s
		
	@samplePoints.setter
	def samplePoints(self, Spoints):
		s = self.instr.write("MPT {}".format(Spoints))
		
	@property
	def singleSweep(self):
		s = self.instr.write("SSI")
	
	@property
	def repeatSweep(self):
		s = self.instr.write("SRT")
		
	#This method gets the data that is displayed on the OSA. It returns a list of powers, so in order to plot this data
	#You will need a list of wavelengths corresponding to what is on the OSA. You can do this by using the Span and CenterWL commands
	#Or just hardcode the ranges in from what you can see on the OSA. The amount of data you get is equal to the sampling points
	@property
	def getData(self):
		data = self.instr.query("DMA?")
		
		#Split results by line
		data = data.split('\r\n')
		
		#Convert unicode to int
		for count,val in enumerate(data):
			try:
				data[count] = float(val)
			except ValueError:
				#This is just an empty string, delete it
				pass
		#Remove the last value as this is always trash
		del data[-1]
			
		return data
		
		
#Class for the AQ6375B Yokogawa Optical Spectrum Analyser
class AQ6375B(object):
	def __init__(self, rm):
			#Have hard coded address of OSA, can look at GPIO address on instrument
			self.instr = rm.get_instrument("GPIB0::2::INSTR")
			#Set the time out to 30 seconds as reading data takes a long time
			self.instr.timeout = 30000
	
	@property
	def singleSweep(self):
		s = self.instr.write(":INIT:SMOD SING; *CLS; :INIT")

	def saveData(self, filename):
		s = self.instr.write(":MMEM:STOR:DATA \"{}\", internal".format(filename))

	def getData(self, filename):
		s = self.instr.query(":MMEM:DATA? \"{}.csv\",internal".format(filename))
		return s
		
	def saveDataLocally(self, filename, localFileName):
		#Gets data from filename on OSA
		s = self.instr.query(":MMEM:DATA? \"{}.csv\",internal".format(filename))
		#Split return unicode into string
		dataList = s.split('\r\n')
		
		#Opens localFileName for writing
		pass
	
class ANDOOSA(object):
	def __init__(self, rm):
		#Have hard coded address of OSA, can look at GPIO address on instrument
		self.instr = rm.get_instrument("GPIB0::1::INSTR")
		#Set the time out to 30 seconds as reading data takes a long time
		self.instr.timeout = 30000
		
	def idn(self):
		s = self.instr.query("idn")
		return s
		
	def singleSweep(self):
		s = self.instr.write("SGL")
		return s
		
	def getAData(self):
		s = self.instr.query("LDATA R1-R1001")
		return s
		
	@property
	def centerWL(self):
		s = self.instr.query("CTRWL?")
		return s
		
	@centerWL.setter
	def centerWL(self, center):
		s = self.instr.write("CTRWL {}".format(center))
	
	@property
	def samplePoints(self):
		s = self.instr.query("SMPL?")
		return s
		
	@samplePoints.setter
	def samplePoints(self, sp):
		s = self.instr.write("SMPL {}".format(sp))
		
	@property
	def span(self):
		s = self.instr.query("SPAN?")
		
	@span.setter
	def span(self, span):
		s = self.instr.write("SPAN {}".format(span))
	
	def getWavelengths(self):
		stopWL = float(self.instr.query("STPWL?"))

		startWL = float(self.instr.query("STAWL?"))

		#WARNING! THIS SETS SAMPLE POINTS BEFORE READING!
		self.samplePoints = 1001
		samplingPoints = self.samplePoints

		wavelengths = np.linspace(startWL, stopWL, samplingPoints)
		return wavelengths
		
class lecroyWaverunner(object):

	#Single scan
	#Set horizontal axis limits
	#Set vols/div
	#Sample points
	#Autoscan
	#getXvals
	#getYvals	
	
	
	def __init__(self):
		self.instr = vxi11.Instrument('169.254.166.206')
		print dir(self.instr)
		
		self.instr.write('CFMT DEF9,BYTE,BIN')
		self.instr.write('CHDR SHORT')
		
	def idn(self):
		return self.instr.ask("*IDN?")
		
	def singleScan(self):
		s = self.instr.ask("""VBS? 'app.Acquisition.Acquire 1, True'""")
		print type(s)
		return s
		
	def getYVals(self):
		#s = self.instr.ask("""VBS? 'return=app.Acquisition.C1.Out.Result'""")
		self.singleScan()
		self.instr.write("C2:WF? DAT1")
		data = self.instr.read_raw()
		return data[data.find('#')+11:-1]
		
	def saveYVals(self, fileName):
		data = self.getYVals()

		f = open(fileName, 'wb')                   # Save data
		f.write(data)
		f.close()

	def instrumentID(self):
		s = self.instr.ask("VBS? return=app.instrumentID")
		return s[4:]
		
	def getXVals(self):
		s = self.instr.ask("")


#Note that this function runs if you run the instrumentClasses.py from the command window.
#You should be importing this library, so the code below is just for testing
if __name__ == "__main__":
	
	print 'started tests'
	#rm = visa.ResourceManager()
	
	scope = lecroyWaverunner()
	
	idn = scope.idn()
	print idn
	
	c = scope.instrumentID()
	print c	
	'''
	for i in range(50):
		b = scope.singleScan()
		print b
		time.sleep(0.5)
	'''
	d = scope.getYVals()
	
	scope.saveYVals("C:\Users\lbab853\Desktop\PhDCode\data\scan1")
	
	print 'finish'
	
	
	
	'''
	
	#Create an ANDO scope
	ando = ANDOOSA(rm)
	
	print ando.centerWL
	
	
	b = ando.singleSweep()
	print b
	
	time.sleep(2)
	
	c = ando.getAData()
	print c
	'''
	
	
	
	#a = ando.saveMem()
	#print a
	#c = ando.recallMem()
	#print c
	
	
	'''
	#Create a LD controller
	LD = ITC4001(rm)
	
	print 'Checking initial settings...'
	print "LD State: {}".format(LD.LD_state)
	print "TEC State: {}".format(LD.TEC_state)
	print "LD Mode: {}".format(LD.LD_mode)

	time.sleep(5)
	
	print 'Turning on TEC and LD...'
	LD.TEC_state = 1
	LD.LD_state = 1
	

	time.sleep(5)
	print "LD State: {}".format(LD.LD_state)
	print "TEC State: {}".format(LD.TEC_state)
	print "LD Mode: {}".format(LD.LD_mode)
	
	print 'Changing current output...'
	for i in np.linspace(0,0.3,20):
		LD.LD_current = i
		time.sleep(3)
	time.sleep(5)
	print 'Returning instrument to idle'
	LD.idle
	'''