#Authored by Logan Baber, 9 May 2016

'''This example script shows a practical use for the instrument classes. It uses a LaserDiode and a PowerMeter.
It makes the Laser change current slowly between two bounds (In this case between 0A and 0.8A), while recording the output values
from the power meter. I have used this for checking the efficiency of a laser (How much pump power is converted to output power)'''

#Import Statements
import instrumentClasses
import visa
import time
import numpy as np
import matplotlib.pyplot as pp

#Create a visa resource manager object
rm = visa.ResourceManager()

#Create a LaserDiode Object
LD = instrumentClasses.ITC4001(rm)

#Create a PowerMeter Object
PM = instrumentClasses.FPM8210(rm)

#Set the LD up
LD.TEC_state = 1
LD.LD_state = 1

#For each current, measure a corresponding output power.
#the conversion between the two gives the efficiency
powerList = []		#This creates a list in python (Array if you'd like)
currentList = np.linspace(0,0.8,100)	#Create a list of 100 numbers, linearly spaced from 0 to 0.8
for i in currentList:					#For every value in the list of 100 numbers:
	LD.LD_current = i					#Set the current to the value
	time.sleep(2)						#Wait for 2 seconds so that the LD can change
	powerList.append(PM.power)			#Append the PowerMeter power to the list

#Calculate a linear equation for the curve
fit = np.polyfit(currentList, powerList, 1)
fit_fn = np.poly1d(fit)		#This is a function that takes a value of current, and spits out a value of power

#Plot Power against Current
#See Matplotlib.pyplot resources on google
fig,ax = pp.subplots(nrows=1, ncols=1)
ax.text(min(currentList) + (max(currentList)-min(currentList))/10, max(powerList) - (max(powerList)-min(powerList))/10, 'Y = {}X + {}'.format(round(fit[0],6), round(fit[1],6)), fontsize=18)
ax.plot(currentList,powerList, 'ro')
ax.plot(currentList, fit_fn(currentList), 'b-')
ax.set_title('Output power Against Pump Current')
ax.set_ylabel('Power (W)')
ax.set_xlabel('Current (A)')
pp.show()

#Saves file (Note I have commented this out as your computer will have differnt paths, but feel free to change this)
'''filename = 'C:\Users\Logan\Documents\Paper\SingleLogitudinalMode\PowerEfficiency\WVsA.png'
print 'Saving file to: {}, Make sure its not getting overridden...'.format(filename)
raw_input('Press enter to continue...')
fig.savefig(filename)
pp.close(fig)'''

#Turn LD off
LD.idle()