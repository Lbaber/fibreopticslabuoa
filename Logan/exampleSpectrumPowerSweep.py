#Authored by Logan Baber, 9 May 2016

'''This example script shows a practical use for the instrument classes. It uses a LaserDiode and an OpticalSpectrumAnalyser.
It sets the Laser current to different values between a range (0 and 0.7A in this case), and then gets the data from the OSA before plotting it.
It saves the plots in a file. This will be a different location for you, so you will need to change this.'''

#Import Statements
import instrumentClasses
import visa
import time
import numpy as np
import matplotlib.pyplot as pp

#Create a visa resource manager object
rm = visa.ResourceManager()

#Create a LaserDiode Object
LD = instrumentClasses.ITC4001(rm)

#Create an OpticalSpectrumAnalyser Object
OSA = instrumentClasses.MS9710(rm)

#Set up the LD
LD.TEC_state = 1
LD.LD_state = 1

#Get information about the minimum and maximum wavelengths
minWL = OSA.centerWavelength - (OSA.span/2)
maxWL = OSA.centerWavelength + (OSA.span/2)

#For each set power, get a spectrum
for count,i in enumerate(np.linspace(0,0.7,20)):
	LD.LD_current = i
	time.sleep(2)
	OSA.singleSweep
	time.sleep(3)
	data = OSA.getData
	time.sleep(5)
	fig, ax = pp.subplots(nrows=1, ncols=1)
	ax.plot(np.linspace(minWL, maxWL, OSA.samplePoints),data)
	ax.set_title('Spectrum at current {}A'.format(i))
	ax.set_ylabel('Power (dBm)')
	ax.set_xlabel('Wavelength (nm)')
	fig.savefig('C:\Users\Sack\Documents\Paper\SingleLogitudinalMode\OSAscans\Spect{}.png'.format(count))
	pp.close(fig)
	
LD.idle