#Authored by Logan Baber, 9 May 2016

'''This example script shows a practical use for the instrument classes. It uses a LaserDiode and an OpticalSpectrumAnalyser.
It sets the Laser current, and then gets the data from the OSA before plotting it.'''


#Import Statements
import instrumentClasses
import visa
import time
import numpy as np
import matplotlib.pyplot as pp

#Create a visa resource manager object
rm = visa.ResourceManager()

#Create a LaserDiode Object
LD = instrumentClasses.ITC4001(rm)

#Create an OpticalSpectrumAnalyser Object
OSA = instrumentClasses.MS9710(rm)

#Set up the LD
LD.TEC_state = 1
LD.LD_state = 1

#Change LD current
LD.LD_current = 0.45

#Wait for it to change
time.sleep(5)

#Get the OSA to run a single sweep
OSA.singleSweep
time.sleep(3)

#Get data from OSA
data = OSA.getData
time.sleep(5)

#Get information about the minimum and maximum wavelengths
minWL = OSA.centerWavelength - (OSA.span/2)
maxWL = OSA.centerWavelength + (OSA.span/2)

#Plot data
fig, ax = pp.subplots(nrows=1, ncols=1)
ax.plot(np.linspace(minWL, maxWL, OSA.samplePoints),data)
ax.set_title('Spectrum SingleSweep')
ax.set_ylabel('Power (dBm)')
ax.set_xlabel('Wavelength (nm)')
pp.show()

#Turn LD off
LD.idle