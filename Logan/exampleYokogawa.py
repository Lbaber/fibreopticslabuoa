#This is an example script on how to use the Yokogawa OSA.

import instrumentClasses
import visa
import time
import matplotlib.pyplot as pp
#Create a visa resource manager object
rm = visa.ResourceManager()

#Create a new Yokogawa OSA object
YOSA = instrumentClasses.AQ6375B(rm)
'''
#Wait for 3 seconds
time.sleep(3)

#Do a single sweep
YOSA.singleSweep
time.sleep(3)
#Save sweep to a datafile 
YOSA.saveData('test002')
time.sleep(3)
'''
#Get the data out
a = YOSA.getData('test002')

b = a.split('\r\n')

#Assign header information
header = b[:12]
data = b[12:-1]
print data[-10:]
print type(data)
freq = []
pow = []
for val in data:
	print val
	#convert to string
	#print val
	val = str(val)
	#print val
	val = val.split(',')
	freq.append(val[0])
	pow.append(val[1])
	
pp.plot(freq, pow)
pp.show()