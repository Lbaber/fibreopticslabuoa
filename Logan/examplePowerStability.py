#Authored by Logan Baber, 9 May 2016

'''This example script shows a practical use for the instrument classes. It uses a LaserDiode and a PowerMeter.
It sets the Laser current set point, while recording the output values over the range of an hour.
I have used this for checking the stability of a laser (Its change in power over time)'''

#Import Statements
import instrumentClasses
import visa
import time
import numpy as np
import matplotlib.pyplot as pp

#Create a visa resource manager object
rm = visa.ResourceManager()

#Create a LaserDiode Object
LD = instrumentClasses.ITC4001(rm)

#Create a PowerMeter Object
PM = instrumentClasses.FPM8210(rm)

#set the power diode power
LD.LD_current = 0.45

#Set the LD up
LD.TEC_state = 1
LD.LD_state = 1

#Set the PM filter response to medium
PM.filter = 'Medium'

#Get several output powers over a range of times
powerList = []
timeList = range(3600)
for i in timeList:
	powerList.append(PM.power)
	time.sleep(1)
	print i
	
#Plots result
fig, ax = pp.subplots(nrows=1, ncols=1)
ax.plot(timeList,powerList)
ax.set_title('Power Variation over time')
ax.set_ylabel('Power (W)')
ax.set_xlabel('Time (s)')
pp.show()

#Turn LD off
LD.idle()