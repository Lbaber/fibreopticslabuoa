#Authored by Logan Baber, 9 May 2016
'''This is a short, easy, python script which will show you how to create instrument objects for use.
By the end of this lesson, you will know how to:

Create an instrument object
Access an instruments properties
Change an instruments properties

We will start by importing our instrumentClasses'''

#Import statements

#This is the import statement to import the instrumentClasses in the same folder as this test script
import instrumentClasses
#visa is short for Virtual Instrument Software Architecture. This is the library we use to talk to our instruments
import visa
#time is a library so that our script can take breaks and wait (So we give time for the LaserDiode to change current)
import time

'''next, we will create a resource manager object. This contains all the possible instruments we can connect to with our computer
Note that you only need to create one resource manager object, and then pass this object to the instrumentClasses'''

#Create a visa resource manager object
rm = visa.ResourceManager()

'''Once we have created our rm object, we can create a instrument object that we can talk to. In this example, we will talk to a Laser Diode
Note that we have to pass the rm object to the ITC4001 class for it to be initialized'''
#Create a LaserDiode object
LaserDiode = instrumentClasses.ITC4001(rm)

'''At this point in the script, we have created the object, now we would like to access the properties of the instrument
For a laserDiode, in this example we will turn the TEC on (So it doesn't over heat), Turn the Laser on, set the laser current, and read the laser current'''

'''In this section, I will go over how to access the properties of an instrument.
This is for getting calls back from the instrument, rather than setting values'''

#Ask the LaserDiode what the LaserState is (If it's on or off)
LaserState = LaserDiode.LD_state
print 'The LaserState of the Laser is {}'.format(LaserState)

#Ask the LaserDiode what the TECState is (If it's on or off)
print 'The TECState of the Laser is {}'.format(LaserDiode.TEC_state)

'''note that we have created a variable called LaserState, and assigned this to the LaserDiode.LD_state
Because we are using @properties in our classes (Google python properties if you are interested), we do NOT use brackets to access these values.
You can assign the state to a variable as in the LaserState example, or you can just reference it immediately as in the TEC example'''

'''Now we would like to change the value of these properties. By changing the TEC_state and LD_state Property to ON, we can turn our TEC and laser on'''

print 'Turning the TEC on...'
LaserDiode.TEC_state = 'ON'

#Wait for 5 seconds so that the TEC can initialize
print 'Waiting for 5 seconds'
time.sleep(5)

print 'Turning the Laser on...'
LaserDiode.LD_state = 'ON'

'''Note that we have used the exact same property of the class (LD_state), but we have used an equals sign. We are assigning our properties value
To what we have on the right side of the equals sign. We have passed in a string of ON, which will turn on our LD and TEC
To make sure that we have correctly changed the states to on, we can observe the green LEDs on the physical instrument, and we can also ask
the instrument what the property is set to again (This is the exact same code as above)'''

#Ask the LaserDiode what the LaserState is (If it's on or off)
LaserState = LaserDiode.LD_state
print 'The LaserState of the Laser is {}'.format(LaserState)

#Ask the LaserDiode what the TECState is (If it's on or off)
print 'The TECState of the Laser is {}'.format(LaserDiode.TEC_state)

'''Great! So we have turned on our laser. We would now like to change the current to different values.'''
#Ask the diode what it's current set point is. 
print 'The LaserCurrent of the Laser is {}'.format(LaserDiode.LD_current)

#changing the current to 0.2A'''
print 'Setting current to 0.2A'
LaserDiode.LD_current = 0.2

#Wait for 5 seconds so that the Current can change
print 'Waiting for 5 seconds'
time.sleep(5)

#Ask the diode what it's current set point is. 
print 'The LaserCurrent of the Laser is {}'.format(LaserDiode.LD_current)

#Wait for 10 seconds so that we can see the Current change on the physical instrument 
print 'Waiting for 10 seconds'
time.sleep(10)

#Once we have finished with our scripts, we should turn the Laser off, so that it is not running as this could be a hazard.
LaserDiode.idle

'''So that is the end of the lesson. I hope you can follow through. Removing all the comments, this script is only 26 lines.
The main points to absorb from this script:

You need to create a resource manager, and pass this object as an argument to the instrumentClasses.
You only need one resource manager for multiple instruments

To read values from an instrument, we do NOT use brackets. Just the property name will return the value.

To write values to an instrument, we do NOT use brackets. Just the property name with an equals sign.

Authored by Logan Baber, 9 May 2016'''





