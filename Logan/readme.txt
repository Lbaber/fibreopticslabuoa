FibreOpticsLabUOA Python Instrument classes. Authored by Logan Baber

The file instrumentClasses contains classes to control instruments in the lab. As of May 9th, 2016, we can control the
Laser diode, Power Meter, and OSA.

Please look at (Right click, Open with notepad software) the examples. Start with exampleScript1.py as this has a lot of
comments to help you understand what it happening.

Please do not hesitate to contact me on 0210356266 or email me at LoganRBaber@gmail.com
I should be able to help with any questions or problems.

-------------------------------------------------------------------------------------------------------------------------

Thought I'd include links to some of the remote control books for further implementation of methods.

OSA: https://dl.cdn-anritsu.com/en-au/test-measurement/files/Manuals/Operation-Manual/MS9710B_W1284AE_remote_opm_e_5_0.pdf

PowerMeter: http://www.ilxlightwave.cn/manuals/fiber_optic_power_meter_FPM-8210_manual.pdf

LaserDiode: https://www.thorlabs.com/software/MUC/4000_Series/Manual/Series4000_SPCI_Programmers_Reference_V3.3.pdf

